import {Table, Button} from 'react-bootstrap';
import {CustomPlaceholder} from 'react-placeholder-image';

export default function Admin(){
	return(
		<div className="my-3">
			<Table striped bordered hover>
			  <thead>
			    <tr>
			      <th>#</th>
			      <th>Product</th>
			      <th>Stocks</th>
			      <th>Price</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <CustomPlaceholder 
			      	width={100} 
			      	height={100}
			      	backgroundColor="#123456"			      						      					      					      
			      />
			      <td>Mouse</td>
			      <td>500</td>
			      <td>$60.00</td>
			    </tr>
			    <tr>
			      <CustomPlaceholder 
			      	width={100} 
			      	height={100}
			      	backgroundColor="#123456"			      						      					      					      
			      />
			      <td>Keyboard</td>
			      <td>600</td>
			      <td>$60.00</td>
			    </tr>
			    <tr>
			      <CustomPlaceholder 
			      	width={100} 
			      	height={100}
			      	backgroundColor="#123456"			      						      					      					      
			      />
			      <td>Headset</td>
			      <td>300</td>			      
			      <td>$60.00</td>
			    </tr>
			  </tbody>
			</Table>
			<Button variant="primary" className="pr-auto">Save</Button>{' '}
		</div>
	)
}