import {Fragment} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';
import {useState, useEffect} from 'react';
import AppNavbar from './components/AppNavbar';
import Register from './pages/Register';
import Login from './pages/Login';
import Product from './pages/Product';
import Admin from './pages/Admin';

import './App.css';

function App() {
  return (
    <Fragment>
        <Router>
          <AppNavbar />
          <Container>
            <Switch>             
              <Route exact path="/login" component={Login} />
              <Route exact path="/product" component={Product} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/admin" component={Admin} />                                        
            </Switch>
          </Container>
        </Router>
      </Fragment>
  );
}

export default App;
