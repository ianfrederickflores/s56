import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {NavLink} from 'react-router-dom';
import {Container, Row, Col, Form, FormControl, Button} from "react-bootstrap";
import {CustomPlaceholder} from 'react-placeholder-image';

export default function AppNavbar(){
	return(
		<Navbar bg="dark" variant="dark">
		    <Container fluid>
		    	<Row>
		    		<Col>
		    			<Navbar.Brand href="#home">
		    				<CustomPlaceholder 
		    					width={200} 
		    					height={50}
		    					backgroundColor="#123456"
		    					textColor="#ffffff"
		    					text="Company Logo"				      
		    				/>
		    			</Navbar.Brand>
		    		</Col>
		    		<Col xs={6}>
		    			<Form className="d-flex">
		    			        <FormControl
		    			          type="search"
		    			          placeholder="Search"
		    			          className="me-2"
		    			          aria-label="Search"
		    			        />
		    			        <Button variant="outline-success">Search</Button>
		    			</Form>
		    		</Col>
		    		<Col xs={1}>
		    			<Nav className="ml-auto">
		    			  <Nav.Link as={NavLink} to="/product" exact>Product</Nav.Link>
		    			  <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
		    			  <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		    			</Nav>
		    		</Col>
		    	</Row>		    		    		    
		    </Container>
		  </Navbar>
	)
}
