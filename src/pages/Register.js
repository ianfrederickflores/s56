import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import {Redirect, useHistory} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register(){
		const [name, setName] = useState('');
		const [email, setEmail] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');		
		const [isActive, setIsActive] = useState(false);
		
		const history = useHistory();		

		function registerUser(e) {

		        e.preventDefault();		        

		        fetch('http://localhost:4000/users/register', {
		            method: "POST",
		            headers: {
		                'Content-Type': 'application/json'
		                },
		            body: JSON.stringify({
		                name: name,
		               	email: email,
		                password: password1
		            })
		        })
		        .then(res => res.json())
		        .then(data => {
		            console.log(data);

		            if(data === true){

		                setName('');
		                setEmail('');
		                setPassword1('');
		                setPassword2('');

		                Swal.fire({
		                    title: 'Registration successful',
		                    icon: 'success',
		                    text: 'Welcome'
		                    });

		                    history.push("/login");

		            } 

		        });
		}

		useEffect(() => {
			if((name !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2))
			{
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		}, [name, email, password1, password2])

	return(		
		<Form onSubmit={(e) => registerUser(e)}>
		  <Form.Group className="mt-3 mb-3" controlId="formBasicName">
		    <Form.Label>Name</Form.Label>
		    <Form.Control 
		        type="name" 
		        placeholder="Enter Name" 
		        value={name}
		        onChange={(e) => setName(e.target.value)}
		        required
		    />		    
		  </Form.Group>

		  <Form.Group className="mt-3 mb-3" controlId="formBasicEmail">
		    <Form.Label>Email address</Form.Label>
		    <Form.Control 
		    	type="email" 
		    	placeholder="Enter email"
		    	value={email}
		    	onChange={ e => setEmail(e.target.value)} 
		    	required 
		    />		    
		  </Form.Group>

		  <Form.Group className="mt-3 mb-3" controlId="formBasicPassword1">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password"
		    	value={password1}
		    	onChange={ e => setPassword1(e.target.value)} 
		    	required 
		    />
		  </Form.Group>

		  <Form.Group className="mt-3 mb-3" controlId="formBasicPassword2">
		    <Form.Label>Verify Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Verify Password"
		    	value={password2}
		    	onChange={ e => setPassword2(e.target.value)}  
		    	required 
		    />
		  </Form.Group>
		  		  
		  {isActive ? 
		  	<Button variant="primary" type="submit" id="submitBtn">
		  		Register
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  		Register
		  	</Button>
		   }

		</Form>
	)
}