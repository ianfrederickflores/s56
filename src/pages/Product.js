import {Container, Row, Col, Carousel, Card, Button} from 'react-bootstrap';
import {CustomPlaceholder} from 'react-placeholder-image';


export default function Product(){
	return(
		<Container  fluid className="my-3">
			<Row>
				<Col>
					<Carousel>
					  <Carousel.Item>
					    <CustomPlaceholder 
					    	width={200} 
					    	height={200}
					    	backgroundColor="#123456"
					    	textColor="#ffffff"
					    	text="Hello World!"					      					      					      
					    />					    
					  </Carousel.Item>
					  <Carousel.Item>
					    <CustomPlaceholder 
					    	width={200} 
					    	height={200}
					    	backgroundColor="#123456"
					    	textColor="#ffffff"
					    	text="Hello World!"					      					      					      
					    />					    
					  </Carousel.Item>
					  <Carousel.Item>
					    <CustomPlaceholder 
					    	width={200} 
					    	height={200}
					    	backgroundColor="#123456"
					    	textColor="#ffffff"
					    	text="Hello World!"					      					      					      
					    />					    
					  </Carousel.Item>
					</Carousel>
				</Col>
				<Col>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dui libero, pretium a pellentesque vitae, finibus vel mauris.
					</p>
					<CustomPlaceholder 
						width={100} 
						height={100}
						backgroundColor="#123456"
						textColor="#ffffff"
						text="Hello World!"					      					      					      
					/>														
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dui libero, pretium a pellentesque vitae, finibus vel mauris.
					</p>
				</Col>
				<Col>
					<Card border="primary" style={{ width: '18rem' }}>					  
					  <Card.Body>					    
					    <Card.Text className="mx-auto">
					      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dui libero, pretium a pellentesque vitae, finibus vel mauris.
					    </Card.Text>
					    <Button variant="primary" className="mx-auto">Add to Cart</Button>
					  </Card.Body>
					</Card>
				</Col>				
			</Row>
		</Container>
	)
}